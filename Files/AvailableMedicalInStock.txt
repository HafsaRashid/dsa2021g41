Distilled Water,500,July 2020,April 2024
Disposible Sterile Gloves,750,April 2021,December 2029
Scissors,1050,N/A,N/A
Calpol,700,July 2020,April 2029
Dexamol,800,July 2020,April 2025
Dolex,900,July 2020,April 2024
Gripin,1000,July 2020,April 2026
Pamol,200,December 2021,April 2030
Panadol,20000,June 2020,April 2026
Panamax,500,June 2020,April 2024
Zolben,500,January 2020,April 2024
Adhesive Tape,30000,N/A,N/A
Triangular Bandage,30000,N/A,N/A
Tweezers,30000,N/A,N/A
Pocket Mask for CPR,30000,N/A,N/A
BVM,30000,N/A,N/A
Suction Unit,30000,N/A,N/A
Stethoscope,30000,N/A,N/A
Blood Pressure Cuff,30000,N/A,N/A
I.V. Kit,30000,N/A,N/A
N95 Mask,30000,N/A,N/A
Airways,30000,N/A,N/A
Cold Packs,30000,N/A,N/A
Carry Chair,30000,N/A,N/A
DisInfectant Solution,30000,N/A,N/A