﻿using ProjectFinal.Data_Structures;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectFinal
{
    public partial class Confirmation_Service : Form
    {
        List<User> user = new List<User>();
        LinkedList l = new LinkedList();
        List<Vehicle> temp = new List<Vehicle>();
        Node n = new Node();
        Stack s = new Stack();
        int count = 0;
        public Confirmation_Service()
        {
            InitializeComponent();
            loaddat();
            addIntoStack();
            LoadUserData();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < user.Count;i++)
            {
                AssignVehicle(user[i].Condition , count);
            }
        }
        public void loaddat()
        {
            
            try
            {

                using (var f = new StreamReader(@"c:\\users\\khadija\\desktop\\dsa2021g41\\files\\ambulancerecord.txt"))
                {
                    string line = string.Empty;
                    while ((line = f.ReadLine()) != null)
                    {
                        Vehicle v = new Vehicle();
                        var s = line.Split(',');
                        v.typeofVehicle = s[0];
                        v.Number = s[1];
                        v.slotNumber = int.Parse(s[2]);
                        v.peoplesAllowed = int.Parse(s[3]);
                        count++;
                        l.insertAmb(v);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
            }
        }
        public void addIntoStack()
        {
            n = l.head;
            while (n != null)
            {
                s.push(count, n.Data);
                n = n.Next;
            }
        }
        public void peekStack()
        {
            Vehicle V = new Vehicle();
            V = s.peek();
        }
        
        public void LoadUserData()
        {
            try
            {

                using (var f = new StreamReader(@"c:\\users\\khadija\\desktop\\dsa2021g41\\files\\Patient.txt"))
                {
                    string line = string.Empty;
                    while ((line = f.ReadLine()) != null)
                    {
                        User p = new User();
                        var s = line.Split(',');
                        p.Email = s[0];
                        p.Name = s[1];
                        p.location = s[2];
                        p.Condition = s[3];
                        user.Add(p);
                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
            }
        }
        public void AssignVehicle (string Condition, int n)
        {
            int count = n;
            Vehicle V = new Vehicle();
            if (Condition.Equals("Critical") || Condition.Equals("Not critical"))
            {
                if (s.peek().typeofVehicle.Equals("Car"))
                {
                    V = s.peek();
                }
                else
                {
                    while (!(s.pop().typeofVehicle.Equals("Car")))
                    {
                        V = new Vehicle();
                        V = s.pop();
                        temp.Add(V);
                    }

                }

            }
            else
            {
                if (Condition.Equals("Small injury"))
                {
                    if (s.peek().typeofVehicle.Equals("Bike"))
                    {
                        V = s.peek();
                    }
                    else
                    {
                        while (!(s.pop().typeofVehicle.Equals("Bike")))
                        {
                            V = new Vehicle();
                            V = s.pop();
                            temp.Add(V);
                        }

                    }
                }
            }
            //Storing back into the stack from temporary storing Stack
            int s1 = temp.Count;
            while (!(s1 == -1))
            {
                if (s1 >= 1)
                {
                    s.push(count, temp[s1 - 1]);
                }
                s1 = s1 - 1;

            }
            MessageBox.Show("V: " + V.typeofVehicle);
        }




    }
}
