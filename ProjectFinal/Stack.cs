﻿using System;
using ProjectFinal.Data_Structures;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Windows.Forms;

namespace ProjectFinal
{
    class Stack
    {
        
        public List<Vehicle> list = new List<Vehicle>();
        private List<Vehicle> temp = new List<Vehicle>();
        public int top = -1;
        public int tempC = -1;
        public void push(int count, Vehicle V)
        {
            int size = count;
            if( top == size - 1)
            {
                MessageBox.Show("out of index");
            }
            else
            {
                top = top + 1;
                list.Add(V);
                list[top] = V;
            }
            
        }
        public Vehicle peek()
        {
            Vehicle v = new Vehicle();

            if (top == -1)
            {
                MessageBox.Show("empty");
            }
            else
            {
                v = list[top];
            }
            return v;
        }
        public Vehicle pop()
        {
            Vehicle V = new Vehicle();
            V = list[top];
            top--;
            return V;
        }
        
    }
}
