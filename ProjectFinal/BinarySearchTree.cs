﻿using System;
using ProjectFinal.Data_Structures;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectFinal
{
    class BinarySearchTree
    {

        Node n = new Node();
        public Node root;
        public BinarySearchTree()
        {
            root = null;
        }
        public void insert(MedicalSupplements M)
        {
            n = new Node();
            n.medData = M;
            Node parent = null;
            Node Next= this.root;

            while (Next!=null)
            {
                parent = Next;
                if (Next.medData.quantity> n.medData.quantity)
                {
                    Next = Next.Left;
                }
                else
                {
                    Next = Next.right;
                }
            }
            if (this.root == null)
            {
                this.root = n;
                return;
            }
            if (parent.medData.quantity < n.medData.quantity)
            {
                parent.right = n;
            }
            else
            {
                parent.Left = n;
            }


        }
        public void printInordr(Node root)
        {
                if (root != null)
                {
                    printInordr(root.Left);
                    MessageBox.Show( root.medData.name + " " + root.medData.quantity + " " + root.medData.exp + " " + root.medData.mfg);
                    printInordr(root.right);
                }

        }
        //public void printInorder()
        //{
        //    printInordr(this.root);
        //}
    }

}

