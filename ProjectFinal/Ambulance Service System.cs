﻿using System;
using System.Collections.Generic;
using System.IO;

using System.Windows.Forms;


namespace ProjectFinal
{
    class Ambulance_Service_System
    {
        Queue<Person> UserInfo = new Queue<Person>();
        private static Ambulance_Service_System instance = null;
        Admin ad = new Admin();
        public BinarySearchTree Bst = new BinarySearchTree();

        private Ambulance_Service_System()
        {

        }
        public static Ambulance_Service_System getInstance()
        {

            if (instance == null)
            {
                //MessageBox.Show("Calling load function");
                instance = new Ambulance_Service_System();
                return instance;
            }

            return instance;

        }
        public void add(Person p)
        {
            if (p.getType().Equals("User"))
            {
                UserInfo.Enqueue(p);
            }
        }
        public void Update(Person p, string email, string condition, string Location, string name)
        {
            Queue<Person> temp = new Queue<Person>();
            while (!(UserInfo.Count == 0))
            {
                if (UserInfo.Peek().Email == email)
                {
                    UserInfo.Dequeue();
                    p.Email = email;
                    ((User)p).Condition = condition;
                    ((User)p).location = Location;
                    ((User)p).Name = name;
                    temp.Enqueue(p);
                }
                else
                {
                    temp.Enqueue(UserInfo.Dequeue());
                }
            }
            storing(temp);
        }
        public void Delete(Person p, string email)
        {
            Queue<Person> temp = new Queue<Person>();
            while (!(UserInfo.Count == 0))
            {
                if (UserInfo.Peek().Email == email)
                {
                    UserInfo.Dequeue();
                }
                else
                {
                    temp.Enqueue(UserInfo.Dequeue());
                }
            }
            storing(temp);


        }
        public void storing(Queue<Person> temp)
        {
            if (UserInfo.Count == 0)
            {
                while (!(temp.Count == 0))
                {
                    UserInfo.Enqueue(temp.Dequeue());
                }
            }
        }
        public void print()
        {
            Queue<Person> temp = new Queue<Person>();
            while (!(UserInfo.Count == 0))
            {
                MessageBox.Show("Email of User is: " + UserInfo.Peek().Email + "\n" + "Name of User is: " + ((User)UserInfo.Peek()).Name + "\n" +
                    "Condition of User is: " + ((User)UserInfo.Peek()).Condition + "\n" + "Location of User is: " + ((User)UserInfo.Peek()).location + "\n");
                temp.Enqueue(UserInfo.Dequeue());
            }
            storing(temp);

        }

        //Add Medical Supplements List that Needed
        public void addMedicalSupplements(MedicalSupplements M)
        {
            ad.List.Add(M);
        }
        public List<MedicalSupplements> returnMedicalList()
        {
            return ad.List;
        }
        // Add Medical Supplements List for Procurement Dept
        public void addMedList(ProcurementDepartmentManager pr)
        {
            Bst.insert(pr.M);

        }
        // File Handling 
        public void addRecord(Vehicle V)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(@"C:\\Users\\Khadija\\Desktop\\dsa2021g41\\Files\\AmbulanceRecord.txt", true))
                {
                    file.WriteLine(V.typeofVehicle + "," + V.Number + "," + V.slotNumber + "," + V.peoplesAllowed);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("error", ex);
            }
        }
        public void addEmployeeRecord(Person p)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(@"C:\\Users\\Khadija\\Desktop\\dsa2021g41\\Files\\EmployeeRecord.txt", true))
                {
                    file.WriteLine(((Employee)p).Name + "," + p.Email + "," + ((Employee)p).AmbNumber + "," + ((Employee)p).allowedPeople);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("error", ex);
            }
        }
        public void AddMedicalRequiredRecord(MedicalSupplements M)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(@"C:\\Users\\Khadija\\Desktop\\dsa2021g41\\Files\\EquipmentList.txt", true))
                {
                    file.WriteLine(M.name + "," + M.quantity);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("error", ex);
            }
        }
        public void ClearEmpFile()
        {
            if (!File.Exists(@"C:\\Users\\Khadija\\Desktop\\dsa2021g41\\Files\\EmployeeRecord.txt"))
                File.Create(@"C:\\Users\\Khadija\\Desktop\\dsa2021g41\\Files\\EmployeeRecord.txt");

            TextWriter tw = new StreamWriter(@"C:\\Users\\Khadija\\Desktop\\dsa2021g41\\Files\\EmployeeRecord.txt", false);
            tw.Write(string.Empty);
            tw.Close();
        }
        public void ClearFile()
        {
            if (!File.Exists(@"C:\\Users\\Khadija\\Desktop\\dsa2021g41\\Files\\AmbulanceRecord.txt"))
                File.Create(@"C:\\Users\\Khadija\\Desktop\\dsa2021g41\\Files\\AmbulanceRecord.txt");

            TextWriter tw = new StreamWriter(@"C:\\Users\\Khadija\\Desktop\\dsa2021g41\\Files\\AmbulanceRecord.txt", false);
            tw.Write(string.Empty);
            tw.Close();
        }
        public void addUserRecord(Person p)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(@"C:\\Users\\Khadija\\Desktop\\dsa2021g41\\Files\\Patient.txt", true))
                {
                    file.WriteLine(p.Email+"," + ((User)p).Name + "," + ((User)p).location +"," + ((User)p).Condition );
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("error", ex);
            }
        }

    }
}
