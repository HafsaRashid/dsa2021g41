﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectFinal.Data_Structures
{
    class LinkedList
    {
        public Node head;
        public LinkedList()
        {
            head = null;
        } // constructor
        bool isEmpty()
        {
            return head == null;
        }
        //Insert Function for Ambulance and Employee
        public void insertAmb(Vehicle v)
        {
            Node n = new Node();
            n.Data = v;
            n.Next = null;
            if (head == null)
            {
                head = n;
            }
            else
            {
                Node temp = new Node();
                temp = head;
                while (temp.Next != null)
                {
                    temp = temp.Next;
                }
                temp.Next = n;
            }
        }
        public void insertEmp(Person p)
        {
            Node n = new Node();
            n.personData = p;
            n.Next = null;
            if (head == null)
            {
                head = n;
            }
            else
            {
                Node temp = new Node();
                temp = head;
                while (temp.Next != null)
                {
                    temp = temp.Next;
                }
                temp.Next = n;
            }
        }
        //Print Ambulance Record
        public void printAmbRecord()
        {
            Node n = new Node();
            n = head;
            while (n != null)
            {
                MessageBox.Show(n.Data.typeofVehicle + "\n" + n.Data.Number + "\n" + n.Data.slotNumber + "\n" + n.Data.peoplesAllowed);
                n = n.Next;
            }

        }
        //Delete Function for Ambulance Record
        public void DeleteAmbRecord(string num)
        {
            Node n = new Node();
            n = head;
            if (n.Data.Number == num)
            {
                n.Data = n.Next.Data;
                n.Next = n.Next.Next;
            }
            else
            {
                
                while (n.Next.Data.Number != num)
                {
                    n = n.Next;
                }
                if (n.Next.Next != null && n.Next.Data.Number == num)
                {
                    n.Data = n.Next.Data;
                    n.Next = n.Next.Next;
                }

                else if (n.Next.Next == null && n.Next.Data.Number == num)
                {
                    n.Next = null;
                }

            }
        }
        //Update Function for Ambulance Record
        public void UpdateAmbulanceRecord(string num, Vehicle v)
        {
            Node n = new Node();
            n = head;
            while (n != null)
            {
                if (n.Data.Number == num)
                {
                    n.Data.typeofVehicle = v.typeofVehicle;
                    n.Data.Number = v.Number;
                    n.Data.slotNumber = v.slotNumber;
                    n.Data.peoplesAllowed = v.peoplesAllowed;
                    n.Data = v;
                    n = n.Next;


                }
                else
                {
                    n = n.Next;

                }
            }

        }
        //Update Function for Employee Record
        public void UpdateEmployeeRecord(string email, Person P)
        {
            Node n = new Node();
            n = head;
            while (n != null)
            {
                if (n.personData.Email == email)
                {
                    n.personData.Email = P.Email;
                    ((Employee)n.personData).Name = ((Employee)P).Name;
                    ((Employee)n.personData).allowedPeople = ((Employee)P).allowedPeople;
                    ((Employee)n.personData).AmbNumber = ((Employee)P).AmbNumber;
                    n.personData = P;
                    n = n.Next;
                }
                else
                {
                    n = n.Next;

                }
            }
        }
        //Delete Function for Employee Record
        public void DeleteEmployeeRecord(string email)
        {
            Node n = new Node();
            n = head;
            if (n.personData.Email == email)
            {
                n.personData = n.Next.personData;
                n.Next = n.Next.Next;
            }
            else
            {

                while (n.Next.personData.Email != email)
                {
                    n = n.Next;
                }
                if (n.Next.Next != null && n.Next.personData.Email == email)
                {
                    n.personData = n.Next.personData;
                    n.Next = n.Next.Next;
                }

                else if (n.Next.Next == null && n.Next.personData.Email == email)
                {
                    n.Next = null;
                }

            }
        }
        //Print Employee Record
        public void printEmpRecord()
        {
            Node n = new Node();
            n = head;
            while (n != null)
            {
                MessageBox.Show(n.personData.Email + "\n" + ((Employee)n.personData).Name + "\n" + ((Employee)n.personData).AmbNumber + "\n" + ((Employee)n.personData).allowedPeople);
                n = n.Next;
            }

        }

    }
}
