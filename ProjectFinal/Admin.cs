﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectFinal
{
    class Admin : Person
    {
        private List<MedicalSupplements> list = new List<MedicalSupplements>();
        //getter setter for accessing Medical Supplements List
        public List<MedicalSupplements> List
        {
            get { return this.list; }
            set { this.list = value; }
        }
    }
}
