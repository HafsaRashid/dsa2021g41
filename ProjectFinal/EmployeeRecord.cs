﻿using System;
using ProjectFinal.Data_Structures;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProjectFinal
{
    public partial class EmployeeRecord : Form
    {
        public EmployeeRecord()
        {
            InitializeComponent();
            loaddat();
            LoadIntoTable();
        }
        int indexRow;
        string email;
        LinkedList List = new LinkedList();
        Ambulance_Service_System amb = Ambulance_Service_System.getInstance();

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void EmployeeRecord_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Employee E = new Employee();
            E.Email = textBox2.Text;
            E.Name = textBox1.Text; 
            E.AmbNumber = textBox4.Text;
            E.allowedPeople = int.Parse(textBox3.Text);
            List.insertEmp(E);
            amb.addEmployeeRecord(E);
            setClear();



        }
        private void setClear()
        {
            textBox1.ResetText();
            textBox2.ResetText();
            textBox3.ResetText();
            textBox4.ResetText();
        }
        public void loaddat()
        {
            try
            {

                using (var f = new StreamReader(@"C:\\Users\\Khadija\\Desktop\\dsa2021g41\\Files\\EmployeeRecord.txt"))
                {
                    string line = string.Empty;
                    while ((line = f.ReadLine()) != null)
                    {
                        Employee E = new Employee();
                        var s = line.Split(',');
                        E.Name = s[0];
                        E.Email = s[1];
                        E.AmbNumber = s[2];
                        E.allowedPeople = int.Parse(s[3]);
                        List.insertEmp(E);
                        setClear();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
            }
        }
        public void LoadIntoTable()
        {
            Node n = new Node();
            n = List.head;
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Name"));
            dt.Columns.Add(new DataColumn("Email"));
            dt.Columns.Add(new DataColumn("Assigned Ambulance Slot Number"));
            dt.Columns.Add(new DataColumn("Number of Peoples Allowed"));
            while (n != null)
            {
                DataRow dr = dt.NewRow();
                dr[0] = ((Employee)n.personData).Name;
                dr[1] = n.personData.Email;
                dr[2] = ((Employee)n.personData).AmbNumber;
                dr[3] = ((Employee)n.personData).allowedPeople;
                dt.Rows.Add(dr);
                dataGridView1.DataSource = dt;
                n = n.Next;
            }

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
            textBox2.Text = row.Cells[1].Value.ToString();
            textBox4.Text = row.Cells[2].Value.ToString();
            textBox3.Text = row.Cells[3].Value.ToString();
            email = textBox2.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Employee E = new Employee();
            DataGridViewRow dat = dataGridView1.Rows[indexRow];
            dat.Cells[0].Value = textBox1.Text;
            dat.Cells[1].Value = textBox2.Text;
            dat.Cells[2].Value = textBox4.Text;
            dat.Cells[3].Value = textBox3.Text;
            E.Name = textBox1.Text;
            E.Email = textBox2.Text;
            E.AmbNumber = textBox4.Text;
            MessageBox.Show("email" + email);
            E.allowedPeople = int.Parse(textBox3.Text);
            List.UpdateEmployeeRecord(email,E);
            LoadIntoTable();
            setClear();
            amb.ClearEmpFile();
            Node n = new Node();
            n = List.head;
            while (n != null)
            {
                Employee Emp = new Employee();
                Emp.Name = ((Employee)n.personData).Name;
                Emp.Email = n.personData.Email;
                Emp.AmbNumber = ((Employee)n.personData).AmbNumber;
                Emp.allowedPeople = ((Employee)n.personData).allowedPeople;
                amb.addEmployeeRecord(E);
                n = n.Next;
            }
            loaddat();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int row = dataGridView1.CurrentCell.RowIndex;
            DataGridViewRow dat = dataGridView1.Rows[row];
            string x = dat.Cells[1].Value.ToString();
            List.DeleteEmployeeRecord(x);
            MessageBox.Show("Node deleted successfully");
            dataGridView1.Rows.RemoveAt(row);
            amb.ClearEmpFile();
            Node n = new Node();
            n = List.head;
            while (n != null)
            {
                Employee E = new Employee();
                E.Name = ((Employee)n.personData).Name;
                E.Email = n.personData.Email;
                E.AmbNumber = ((Employee)n.personData).AmbNumber;
                E.allowedPeople = ((Employee)n.personData).allowedPeople;
                amb.addEmployeeRecord(E);
                n = n.Next;
            }
            loaddat();
        }
    }
}
